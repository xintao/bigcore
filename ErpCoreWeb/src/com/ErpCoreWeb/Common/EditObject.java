package com.ErpCoreWeb.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ErpCoreModel.Framework.PO;

public class EditObject {
	static HashMap<String, List<PO>> m_sortEditObject = new HashMap<String, List<PO>>();
    public EditObject()
    {
        //
        //TODO: 在此处添加构造函数逻辑
        //
    }
    static public void Add(String SessionID,PO obj)
    {
        if (m_sortEditObject.containsKey(SessionID))
        {
            m_sortEditObject.get(SessionID).add(obj);
        }
        else
        {
            List<PO> list = new ArrayList<PO>();
            list.add(obj);
            m_sortEditObject.put(SessionID, list);
        }

    }
    static public void Remove(String SessionID, PO obj)
    {
        if (!m_sortEditObject.containsKey(SessionID))
            return;
        m_sortEditObject.get(SessionID).remove(obj);

    }
    static public List<PO> GetList(String SessionID)
    {
        List<PO> list = new ArrayList<PO>();
        if (m_sortEditObject.containsKey(SessionID))
        {
            list = m_sortEditObject.get(SessionID);
        }
        return list;
    }
    //取消所有编辑对象
    static public void Cancel(String SessionID)
    {
        if (m_sortEditObject.containsKey(SessionID))
        {
            List<PO> list = m_sortEditObject.get(SessionID);
            for (int i=0;i< list.size();i++)
            {
                list.get(i).Cancel();
            }
            m_sortEditObject.remove(SessionID);
        }
    }
}
