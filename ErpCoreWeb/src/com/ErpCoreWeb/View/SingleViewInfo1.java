package com.ErpCoreWeb.View;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CView;
import com.ErpCoreModel.UI.CViewDetail;
import com.ErpCoreModel.UI.enumViewType;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class SingleViewInfo1
 */
@WebServlet("/SingleViewInfo1")
public class SingleViewInfo1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CView m_View = null;
    public UUID m_Catalog_id = Util.GetEmptyUUID();
    boolean m_bIsNew = false;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SingleViewInfo1() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
            	response.getWriter().print("请重新登录！");
            	response.getWriter().close();
            	//response.getWriter().print("url:../Login.jsp");
				//response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
		
		String id = request.getParameter("id");
        if (!Global.IsNullParameter(id))
        {
            m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
        }
        else
        {
        	m_bIsNew = true;
            if (request.getSession().getAttribute("NewView") == null)
            {
                m_View = new CView();
                m_View.Ctx = Global.GetCtx(this.getServletContext());
                m_View.setVType ( enumViewType.Single);
                Map<UUID, CView> sortObj = new HashMap<UUID, CView>();
                sortObj.put(m_View.getId(), m_View);
                request.getSession().setAttribute("NewView", sortObj);
            }
            else
            {
            	Map<UUID, CView> sortObj = (Map<UUID, CView>)request.getSession().getAttribute("NewView");
                m_View =(CView) sortObj.values().toArray()[0];
            }
        }
        String catalog_id = request.getParameter("catalog_id");
        if (!Global.IsNullParameter(catalog_id))
        {
            m_Catalog_id = Util.GetUUID(catalog_id);
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("Next"))
        {
        	Next();
            return ;
        }
        else if (Action.equalsIgnoreCase("Cancel"))
        {
        	request.getSession().setAttribute("NewView", null);
            return ;
        }
	}
	void Next()
    {
        if (!ValidatePage1())
            return;
        if (!SavePage1())
            return;

    }
    boolean ValidatePage1()
    {
    	String txtName=request.getParameter("txtName");
    	String cbMasterTable=request.getParameter("cbMasterTable");
        if (Global.IsNullParameter(txtName))
        {
        	try {
				response.getWriter().print("名称不能空！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return false;
        }
        if (Global.IsNullParameter(cbMasterTable))
        {
        	try {
				response.getWriter().print("请选择主表！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return false;
        }
        
        return true;
    }
    boolean SavePage1()
    {
        String sName = request.getParameter("txtName");
        String cbMasterTable = request.getParameter("cbMasterTable");
        
        if (!sName.equalsIgnoreCase(m_View.getName()))
        {
            CView view2 = Global.GetCtx(this.getServletContext()).getViewMgr().FindByName(sName);
            if (view2 != null)
            {
            	try {
					response.getWriter().print("名称重复！");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                return false;
            }
        }
        m_View.setName ( sName);

        m_View.setUI_ViewCatalog_id ( m_Catalog_id);
        //如果是修改，并修改了主表，则清空旧字段
        UUID guidMT = Util.GetUUID(cbMasterTable);
        if (!m_bIsNew && !guidMT.equals( m_View.getFW_Table_id()))
        {
            m_View.getColumnInViewMgr().RemoveAll();
        }
        m_View.setFW_Table_id( guidMT);

        return true;
    }
    
}
