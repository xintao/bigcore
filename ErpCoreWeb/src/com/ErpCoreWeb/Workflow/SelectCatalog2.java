package com.ErpCoreWeb.Workflow;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Workflow.CWorkflowCatalog;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class SelectCatalog2
 */
@WebServlet("/SelectCatalog2")
public class SelectCatalog2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;

    public CCompany m_Company = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectCatalog2() {
        super();
        // TODO Auto-generated constructor stub
    }

    //初始化变量
    void initData()
    {
        if (request.getSession().getAttribute("User") == null)
        {
            try {
				response.sendRedirect("../Login.jsp");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return ;
        }
        
			String B_Company_id = request.getParameter("B_Company_id");
			if (Global.IsNullParameter(B_Company_id))
				m_Company = Global.GetCtx(this.getServletContext())
						.getCompanyMgr().FindTopCompany();
			else
				m_Company = (CCompany) Global.GetCtx(this.getServletContext())
						.getCompanyMgr().Find(Util.GetUUID(B_Company_id));

		
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

		//初始化变量
	    initData();
	    //
        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("GetData"))
        {
        	GetData();
            return ;
        }
	}

    void GetData()
    {
        String pid = request.getParameter("pid");
        UUID Parent_id = Util.GetEmptyUUID();
        if (!Global.IsNullParameter(pid))
            Parent_id = Util.GetUUID(pid);
        //context.Response.Write(@"[{text: '工作流'}]");
        String sJson = "[";
        List<CBaseObject> lstWorkflowCatalog = m_Company.getWorkflowCatalogMgr().GetList();
        for (CBaseObject obj : lstWorkflowCatalog)
        {
            CWorkflowCatalog catalog = (CWorkflowCatalog)obj;
            if (catalog.getParent_id().equals(Parent_id))
            {
                String sItem = String.format("{ isexpand: \"false\",\"id\":\"%s\", name: \"%s\",\"text\": \"%s\", children: [] },",
                    catalog.getId().toString(),
                    catalog.getName());
                sJson += sItem;
            }
        }
		if (sJson.endsWith(","))
			sJson = sJson.substring(0, sJson.length() - 1);
        sJson += "]";
        try {
			response.getWriter().print(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
