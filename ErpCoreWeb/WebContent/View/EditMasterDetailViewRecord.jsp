<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/mytaglib" prefix="cc"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.UI.CColumnInViewDetail" %>
<%@ page import="com.ErpCoreModel.Base.AccessType" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>

<%
if (request.getSession().getAttribute("User") == null)
{
    response.getWriter().close();
    return ;
}
CUser m_User = (CUser) request.getSession().getAttribute("User");

CTable m_Table = null;
CView m_View = null;
int m_iCurPage = 1;
int m_iCurPageSize = 30;

String vid = request.getParameter("vid");
if (Global.IsNullParameter(vid))
{
	response.getWriter().close();
}
m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(vid));
if (m_View==null)
{
	response.getWriter().close();
}
pageContext.setAttribute("View", m_View);

m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());
pageContext.setAttribute("Table", m_Table);

Map<UUID, AccessType> m_sortRestrictColumnAccessType = m_User.GetRestrictColumnAccessTypeList(m_Table);
pageContext.setAttribute("RestrictColumnAccessType", m_sortRestrictColumnAccessType);

int m_iUIColCount=2;
if (!Global.IsNullParameter(request.getParameter("UIColCount")))
	m_iUIColCount = Integer.valueOf(request.getParameter("UIColCount"));
pageContext.setAttribute("UIColCount", m_iUIColCount);

//外面传递的默认值
Map<String, String> m_sortDefVal = new HashMap<String, String>();
for (CBaseObject obj : m_Table.getColumnMgr().GetList())
{
	CColumn col = (CColumn)obj;
    if (!Global.IsNullParameter(request.getParameter(col.getCode())))
        m_sortDefVal.put(col.getCode(), request.getParameter(col.getCode()));
}
pageContext.setAttribute("DefVal", m_sortDefVal);

//隐藏字段
Map<String, String> m_sortHideColumn = new HashMap<String, String>();
String sHideCols=request.getParameter("HideCols");
if (!Global.IsNullParameter(sHideCols))
{
	String[] arr = sHideCols.split(",");
    for (String code : arr)
    {
        m_sortHideColumn.put(code, code);
    }
}
pageContext.setAttribute("HideCols", m_sortHideColumn);

String id = request.getParameter("id");
if (Global.IsNullParameter(id))
{
	response.getWriter().print("请选择记录！");
	response.getWriter().close();
}
if (request.getSession().getAttribute("EditMasterDetailViewRecord") != null)
{
    Map<UUID, CBaseObject> arrP = (Map<UUID, CBaseObject>)request.getSession().getAttribute("EditMasterDetailViewRecord");
    CBaseObject objP = (CBaseObject)arrP.values().toArray()[0];
    if(!objP.getId().toString().equalsIgnoreCase(id))
    	request.getSession().setAttribute("EditMasterDetailViewRecord",null);
}

UUID m_guidParentId=Util.GetEmptyUUID();
String ParentId = request.getParameter("ParentId");
if (!Global.IsNullParameter(ParentId))
    m_guidParentId = Util.GetUUID(ParentId);

if (request.getSession().getAttribute("EditMasterDetailViewRecord") == null)
{
    CBaseObjectMgr BaseObjectMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(m_Table.getCode(), m_guidParentId);
    if (BaseObjectMgr == null)
    {
        BaseObjectMgr = new CBaseObjectMgr();
        BaseObjectMgr.TbCode = m_Table.getCode();
        BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
        String sWhere = String.format(" id='%s'", id);
        BaseObjectMgr.GetList(sWhere);
    }
    CBaseObject obj = BaseObjectMgr.Find(Util.GetUUID(id));
    if (obj == null)
    {
    	response.getWriter().print("请选择记录！");
    	response.getWriter().close();
    }
    Map<UUID, CBaseObject> arrP = new HashMap<UUID, CBaseObject>();
    arrP.put(obj.getId(), obj);
    request.getSession().setAttribute("EditMasterDetailViewRecord", arrP);
}

Map<UUID, CBaseObject> arrP2 = (Map<UUID, CBaseObject>)request.getSession().getAttribute("EditMasterDetailViewRecord");
CBaseObject m_BaseObject = (CBaseObject)arrP2.values().toArray()[0];
pageContext.setAttribute("BaseObject", m_BaseObject);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <script charset="utf-8" src="../kindeditor/examples/jquery.js"></script>
    <!--<script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>-->
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    
    <!--在线编辑器-->
	<link rel="stylesheet" href="../kindeditor/jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.css" />
	<link rel="stylesheet" href="../kindeditor/themes/default/default.css" />
	<script charset="utf-8" src="../kindeditor/jquery-ui/js/jquery-ui-1.9.2.custom.js"></script>
	<script charset="utf-8" src="../kindeditor/kindeditor.js"></script>
	<script charset="utf-8" src="../kindeditor/lang/zh_CN.js"></script>

    <script type="text/javascript">
       

        function onSubmit() {
            document.getElementById("form1").submit();
        } 
        function onCancel() {
            $.post(
                'EditMasterDetailViewRecord.do',
                {
                	vid:'<%=request.getParameter("vid") %>',
                id:'<%=request.getParameter("id") %>',
                ParentId: '<%=request.getParameter("ParentId") %>',
                    Action: 'Cancel'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid.loadData(true);
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        } 
        
        $(function() {

            $("#toptoolbar").ligerToolBar({ items: [
                { text: '增加', click: onAdd, icon: 'add' },
                { line: true },
                { text: '修改', click: onEdit, icon: 'modify' },
                { line: true },
                { text: '删除', click: onDelete, icon: 'delete' }
            ]
            });
        });

        //根据字段类型确定窗体宽度
        var winWidth = 450;
        var winHeight=240;
        
        function onAdd() {
            $.ligerDialog.open({ title: '新建', url: 'AddDetailRecord2.jsp?vid=<%=request.getParameter("vid") %>', name: 'winAddRec', height: winHeight, width: winWidth, showMax: true, showToggle: true, showMin: false, isResize: true, modal: false, slide: false, 
            buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winAddRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    var ret = document.getElementById('winAddRec').contentWindow.onCancel();
                } }
             ], isResize: true
            });
        }
        function onOkAddDetailRecord2(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onCancelAddDetailRecord2(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onEdit() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            $.ligerDialog.open({ title: '修改', url: 'EditDetailRecord2.jsp?vid=<%=request.getParameter("vid") %>&id=' + row.id, name: 'winEditRec', height: winHeight, width: winWidth, showMax: true, showToggle: true, showMin: false, isResize: true, modal: false, slide: false, 
            buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winEditRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    var ret = document.getElementById('winEditRec').contentWindow.onCancel();
                } }
             ], isResize: true
            });
        }
        function onOkEditDetailRecord2(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onCancelEditDetailRecord2(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onDelete() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            $.ligerDialog.confirm('确认删除？', function(yes) {
                if (yes) {
                    $.post(
                    'EditMasterDetailViewRecord.do',
                    {
                        Action: 'Delete',
                        vid: '<%=request.getParameter("vid") %>',
                        ParentId:'<%=request.getParameter("ParentId") %>',
                        delid: row.id
                    },
                     function(data) {
                         if (data == "" || data == null) {
                             $.ligerDialog.close();
                             grid.loadData(true);
                             return true;
                         }
                         else {
                             $.ligerDialog.warn(data);
                             return false;
                         }
                     },
                    'text');
                }
            });
        }
        
        var grid;
        $(function ()
        {
            grid = $("#gridDetail").ligerGrid({
            columns: [
                <%
                CViewDetail ViewDetail = (CViewDetail)m_View.getViewDetailMgr().GetFirstObj();
                CTable table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(ViewDetail.getFW_Table_id());
                List<CBaseObject> lstObjCIVD=ViewDetail.getColumnInViewDetailMgr().GetList();
                for (int i=0;i<lstObjCIVD.size();i++)
                {
                    CColumnInViewDetail civd = (CColumnInViewDetail)lstObjCIVD.get(i);
                    CColumn col = (CColumn)table.getColumnMgr().Find(civd.getFW_Column_id());
                    if (col == null)
                        continue;
                    if(i<lstObjCIVD.size()-1)
                    	out.print(String.format("{ display: '%s', name: '%s'},",col.getName(),col.getCode()));
                    else
                    	out.print(String.format("{ display: '%s', name: '%s'}",col.getName(),col.getCode()));
                }
                 %>
                ],
                url: 'EditMasterDetailViewRecord.do?Action=GetDetail&vid=<%=request.getParameter("vid") %>&ParentId=<%=request.getParameter("ParentId") %>&id=<%=request.getParameter("id") %>',
                dataAction: 'server', 
                usePager: false,
                width: '100%', height: 150,
                onSelectRow: function (data, rowindex, rowobj)
                {
                    //$.ligerDialog.alert('1选择的是' + data.id);
                },
                onUnSelectRow: function (data, rowindex, rowobj)
                {
                    //alert('反选择的是' + data.id);
                }
            });
        });

    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <iframe id="submitfrm" name="submitfrm" style="display: none"></iframe>
    <form id="form1" action="EditMasterDetailViewRecord.do" enctype="multipart/form-data" method="post" target="submitfrm">
    <input type="hidden" name="Action" value="PostData"/>
    <input type="hidden" name="vid" value="<%=request.getParameter("vid") %>"/>
    <input type="hidden" name="id" value="<%=request.getParameter("id") %>"/>
    <input type="hidden" name="ParentId" value="<%=request.getParameter("ParentId") %>"/>
    <div>
        <cc:ViewRecordCtrl  table="${pageScope.Table }" 
         view="${pageScope.View }" 
         restrictColumnAccessType="${pageScope.RestrictColumnAccessType }" 
         uiColCount="${pageScope.UIColCount }" 
         defVal="${pageScope.DefVal }" 
         hideColumn="${pageScope.HideCols }" 
         baseObject="${pageScope.BaseObject }" />
   </div>
  <div id="toptoolbar"></div> 
   <div id="gridDetail" style="margin:0; padding:0"></div>
    </div>
    </form>
    
</body>
</html>