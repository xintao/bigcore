<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewCatalog" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.UI.enumViewType" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
<%@ page import="java.util.HashMap" %>
    
<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CView m_View=null;
CTable m_Table =null;
String id = request.getParameter("id");
boolean m_bIsNew=false;
if (!Global.IsNullParameter(id))
{
    m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
}
else
{
	m_bIsNew = true;
    if (request.getSession().getAttribute("NewView") == null)
    {
    	response.sendRedirect("SingleViewInfo1.jsp?id=" + request.getParameter("id") + "&catalog_id=" + request.getParameter("catalog_id"));
    	return ;
    }
    else
    {
    	Map<UUID, CView> sortObj = (Map<UUID, CView>)request.getSession().getAttribute("NewView");
        m_View =(CView) sortObj.values().toArray()[0];
    }
}

m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function() {
            $("#toptoolbar").ligerToolBar({ items: [
                { text: '向上', click: onUp },
                { text: '向下', click: onDown }
            ]
            });
        });
        //向上
        function onUp() {
            grid.up( grid.getSelectedRow());
        }
        //向下
        function onDown() {
            grid.down(grid.getSelectedRow());
        }
    </script>
    <script type="text/javascript">
        var grid;
        $(function() {
            grid = $("#gridTable").ligerGrid({
                columns: [
                { display: '列名', name: 'ColName', align: 'left', width: 120 },
                { display: '标题', name: 'Caption', align: 'left', width: 120, editor: { type: 'text'} }
                ],
                url: 'SingleViewInfo3.do?Action=GetData&id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %> ',
                dataAction: 'server',
                usePager: false,
                enabledEdit: true,
                width: '100%', height: '80%',
                onBeforeEdit: function(e) {
                },
                onBeforeSubmitEdit: function(e) {
                },
                onAfterEdit: function(e) {
                }
            });
        });


        
        function btPrev_onclick() {
            window.location.href = 'SingleViewInfo2.jsp?id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %> ';
        }

        function btNext_onclick() {
            var postData = "";
            var rowData = grid.getData();
            for (var idx = 0; idx < rowData.length; idx++) {
                var id, caption;
                $.each(rowData[idx], function(key, val) {
                    if (key == "id")
                        id = val;
                    else if (key == "Caption")
                        caption = val;
                });
                postData += id + ":" + caption;
                postData += ";";
            }
            //提交
            $.post(
                'SingleViewInfo3.do',
                {
                    Action: 'PostData',
                    id: '<%=request.getParameter("id") %>',
                    catalog_id: '<%=request.getParameter("catalog_id") %>',
                    GridData: postData
                },
                 function(data) {
                     if (data == "" || data == null) {
                         document.location.href = 'SingleViewInfo4.jsp?id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %> ';
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text'
                 );
        }

        function btCancel_onclick() {
            $.post(
                'SingleViewInfo3.do',
                {
                    id: '<%=request.getParameter("id") %>',
                    catalog_id: '<%=request.getParameter("catalog_id") %>',
                    Action: 'Cancel'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid.loadData(true);
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        }

    </script>
</head>
<body style="padding:6px; overflow:hidden;"> 
    <div>排序：</div>
    <div>主表：<input id="txtTbName" name="txtTbName" value="<%=m_Table.getName() %>" disabled="disabled" /></div>
  <div id="toptoolbar"></div> 
    <div id="gridTable" style="margin:0; padding:0"></div>
    <input id="btPrev" type="button" value="上一步" style="width:60px" onclick="return btPrev_onclick()" />&nbsp;&nbsp;&nbsp;
    <input id="btNext" type="button" value="下一步" style="width:60px" onclick="return btNext_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;
    <input id="btCancel" type="button" value="取消" style="width:60px" onclick="return btCancel_onclick()" />
</body>
</html>