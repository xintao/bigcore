<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CColumnInView" %>
<%@ page import="com.ErpCoreModel.UI.CTButtonInView" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="java.util.UUID" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>

<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CTable m_Table = null;
CView m_View = null;
int m_iCurPage = 1;
int m_iCurPageSize = 30;

String vid = request.getParameter("vid");
if (Global.IsNullParameter(vid))
{
	response.getWriter().close();
}
m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(vid));
if (m_View==null)
{
	response.getWriter().close();
}
m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());

%>
<%!
//其他工具栏按钮
public String GetTButtonInView(CView View)
{
	String sRet = "";
    List<CTButtonInView> lstObj = View.getTButtonInViewMgr().GetListOrderByIdx();
    for (CTButtonInView tiv : lstObj)
    {
        sRet += ",\r\n" + String.format("{ text: '%s', click: onTButton, url:'%s'}", tiv.getCaption(), tiv.getUrl());
    }
    return sRet;
}
public String GetCompanyId(HttpServletRequest request)
{
    CUser user = (CUser)request.getSession().getAttribute("User");
    return user.getB_Company_id().toString();
}
public List<CColumn> GetColList(CView View,CTable Table)
{
    List<CColumn> lstRet = new ArrayList<CColumn>();
    List<CBaseObject> lstObjCIV = View.getColumnInViewMgr().GetList();
    for (int i = 0; i < lstObjCIV.size(); i++)
    {
        CColumnInView civ = (CColumnInView)lstObjCIV.get(i);
        CColumn col = (CColumn)Table.getColumnMgr().Find(civ.getFW_Column_id());
        if (col == null)
            continue;
        lstRet.add(col);
    }
    return lstRet;
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../lib/jquery/jquery.PrintArea.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTab.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script> 

    <script type="text/javascript">
        $(function() {
            $("#toptoolbar").ligerToolBar({ items: [
                { text: '增加', click: onAdd, icon: 'add' },
                { text: '修改', click: onEdit, icon: 'modify' },
                { text: '删除', click: onDelete, icon: 'delete' },
                { line: true },
                { text: '打印', click: onPrint , icon: 'print'},
                { line: true },
                { text: '刷新', click: onRefresh, icon: 'refresh'},
                { line: true },
                { text: '过滤', click: onFilter,icon: 'search'},
                { line: true },
                { text: '工作流', click: onWorkflow, icon: 'settings'},
                { line: true }                
                <%=GetTButtonInView(m_View) %>
            ]
            });
        });

        //打印
        function onPrint(){
            $("#gridTable").printArea();
        }
        //其他工具栏按钮
        function onTButton(item) {
            var caption=item.text;  
            var url=item.url;
            $.ligerDialog.open({ title: caption, url: url, name: 'winTButton', height: 350, width: 500, showMax: true, showToggle: true, isResize: true, modal: false, slide: false
            });
        }
        //刷新
        function onRefresh() {
            var url = 'MultMasterDetailView.do?Action=GetData&vid=<%=request.getParameter("vid") %>&ParentId=<%=request.getParameter("ParentId") %>';
            grid.set({ url: url });
            //grid.loadData();
        }
        //过滤
        function onFilter() {
            $.ligerDialog.open({ title: '过滤', url: 'SetViewFilter.jsp?vid=<%=request.getParameter("vid") %>', name: 'winFilter', height: 350, width: 500, showMax: true, showToggle: true, isResize: true, modal: false, slide: false, 
            buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    $.ligerDialog.close();
                    var url = 'MultMasterDetailView.do?Action=GetData&vid=<%=request.getParameter("vid") %>&ParentId=<%=request.getParameter("ParentId") %>&Filter=1';
                    grid.set({ url: url });
                    //grid.loadData();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    $.ligerDialog.close();
                } }
             ], isResize: true
            });
        }
        //根据字段类型确定窗体宽度
        var winWidth = 600;
        var winHeight=320;
        
        function onAdd() {
            $.ligerDialog.open({ title: '新建', url: 'AddMultMasterDetailViewRecord.jsp?vid=<%=request.getParameter("vid") %>&ParentId=<%=request.getParameter("ParentId") %>', name: 'winAddRec', height: winHeight, width: winWidth, showMax: true, showToggle: true, isResize: true, modal: false, slide: false, 
            buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winAddRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    var ret = document.getElementById('winAddRec').contentWindow.onCancel();
                } }
             ], isResize: true
            });
        }
        function onOkAddMultMasterDetailViewRecord(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onCancelAddMultMasterDetailViewRecord(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onEdit() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            $.ligerDialog.open({ title: '修改', url: 'EditMultMasterDetailViewRecord.jsp?vid=<%=request.getParameter("vid") %>&ParentId=<%=request.getParameter("ParentId") %>&id=' + row.id, name: 'winEditRec', height: winHeight, width: winWidth, showMax: true, showToggle: true, isResize: true, modal: false, slide: false, 
            buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var ret = document.getElementById('winEditRec').contentWindow.onSubmit();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    var ret = document.getElementById('winEditRec').contentWindow.onCancel();
                } }
             ], isResize: true
            });
        }
        function onOkEditMultMasterDetailViewRecord(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onCancelEditMultMasterDetailViewRecord(){
            grid.loadData(true);
            $.ligerDialog.close();
        }
        function onDelete() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            $.ligerDialog.confirm('确认删除？', function(yes) {
                if (yes) {
                    $.post(
                    'MultMasterDetailView.do',
                    {
                        Action: 'Delete',
                        vid: '<%=request.getParameter("vid") %>',
                        ParentId:'<%=request.getParameter("ParentId") %>',
                        delid: row.id
                    },
                     function(data) {
                         if (data == "" || data == null) {
                             $.ligerDialog.close();
                             grid.loadData(true);
                             return true;
                         }
                         else {
                             $.ligerDialog.warn(data);
                             return false;
                         }
                     },
                    'text');
                }
            });
        }
        var dlgMenuWorkflow;
        function onWorkflow() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            if (dlgMenuWorkflow == null) {
                dlgMenuWorkflow = $.ligerDialog.open({ target: $("#menuWorkflow"), width: 130, modal: false });

                $(".l-dialog-close").bind('mousedown', function()  //dialog右上角的叉
                {
                    dlgMenuWorkflow.hide();
                });
            }
            else {
                dlgMenuWorkflow.show();
            }
        }
        function onStartWF() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            dlgMenuWorkflow.hide();
            $.ligerDialog.open({ title: '选择工作流', url: '../Workflow/SelectWorkflowDef.jsp?B_Company_id=<%=GetCompanyId(request) %>&FW_Table_id=<%=m_Table.getId() %>', name: 'winSelWF', height: 200, width: 300, showMax: true, showToggle: true, showMin: true, isResize: true, modal: false, slide: false,
                buttons: [
                { text: '确定', onclick: function(item, dialog) {
                    var fn = dialog.frame.onSelect || dialog.frame.window.onSelect;
                    var data = fn();
                    if (!data) {
                        $.ligerDialog.alert('请选择行!');
                        return false;
                    }
                    $.post(
                    '../Workflow/StartWorkflow.do',
                    {
                        TbCode: '<%=m_Table.getCode() %>',
                        id: row.id,
                        WF_WorkflowDef_id: data.id,
                        ParentId: '<%=request.getParameter("ParentId")%>'
                    },
                     function(data) {
                         if (data == "" || data == null) {
                             grid.loadData(true);
                         }
                         else {
                             $.ligerDialog.warn(data);
                             return false;
                         }
                     },
                    'text');
                    dialog.close();
                }
                },
                { text: '取消', onclick: function(item, dialog) {
                    dialog.close();
                }
                }
             ], isResize: true
            });
        }
        function onViewWF() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            dlgMenuWorkflow.hide();
            $.ligerDialog.open({ title: '查看工作流', url: '../Workflow/ViewWorkflow.jsp?TbCode=<%=m_Table.getCode() %>&ParentId=<%=request.getParameter("ParentId") %>&id=' + row.id, name: 'winViewWF', height: 400, width: 600, showMax: true, showToggle: true,  isResize: true, modal: false, slide: false
            });
        }
        
    </script>
    <style type="text/css">
    #menu1,.l-menu-shadow{top:30px; left:50px;}
    #menu1{  width:200px;}
    </style>
    
    <script type="text/javascript">
        var grid;
        $(function ()
        {
            grid = $("#gridTable").ligerGrid({
            columns: [
                <%
                List<CColumn> lstCol=GetColList(m_View,m_Table);
                for (int i=0;i<lstCol.size();i++)
                {
                    CColumn col = (CColumn)lstCol.get(i);
                    if(i<lstCol.size()-1)
                    	out.print(String.format("{ display: '%s', name: '%s',width:120},",col.getName(),col.getCode()));
                    else
                    	out.print(String.format("{ display: '%s', name: '%s',width:120}",col.getName(),col.getCode()));
                }
                 %>
                ],
                url: 'MultMasterDetailView.do?Action=GetData&vid=<%=request.getParameter("vid") %>&ParentId=<%=request.getParameter("ParentId") %>',
                dataAction: 'server', pageSize: 30,
                width: '100%', height: '50%',
                onSelectRow: function (data, rowindex, rowobj)
                {
                    var url = 'MultDetailList.jsp?vid=<%=request.getParameter("vid") %>&ParentId=<%=request.getParameter("ParentId") %>&pid=' + data.id;
                    var arr = tab.getTabidList();
                    for (var i = 0; i < arr.length; i++) {
                        tab.overrideTabItem(arr[i],{url:url+'&vdid='+arr[i]});
                    }
                }
            });
        });
        
        var tab = null;
        $(function ()
        {                
            //Tab
            $("#tabDetail").ligerTab({ 
            height: 400 ,
            onBeforeSelectTabItem: function (tabid)
            {
                tab.reload(tabid);
            }, 
            onAfterSelectTabItem: function (tabid)
            {
                //alert('onAfterSelectTabItem' + tabid);
            },
            onBeforeRemoveTabItem: function (tabid)
            {
                return false;
            } 
            });
            tab = $("#tabDetail").ligerGetTabManager();
            <%for(CBaseObject obj : m_View.getViewDetailMgr().GetList())
            {
                CViewDetail vd = (CViewDetail)obj;
                %>
                f_addTab('<%=vd.getId() %>','<%=vd.getName() %>','MultDetailList.jsp?vid=<%=request.getParameter("vid") %>&vdid=<%=vd.getId() %>&ParentId=<%=request.getParameter("ParentId") %>');
                <%
            }
             %>
        });
        function f_addTab(tabid,text, url)
        { 
            tab.addTabItem({ tabid : tabid,text: text, url: url });
        } 
    </script>
</head>
<body style="padding:6px; overflow:hidden;">
  <div id="toptoolbar"></div> 
   <div id="gridTable" style="margin:0; padding:0"></div>
    <div position="center" id="tabDetail">
    </div>
    

<div id="menuWorkflow" style=" margin:3px; display:none;">
    <input id="btStartWF" type="button" value="启动工作流" onclick="onStartWF()" style="width:100px; height:30px" /><br />
    <input id="btViewWF" type="button" value="查看工作流" onclick="onViewWF()" style="width:100px; height:30px" />
</div>
</body>
</html>