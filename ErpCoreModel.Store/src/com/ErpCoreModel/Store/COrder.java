﻿// File:    COrder.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:33
// Purpose: Definition of Class COrder

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;
import com.ErpCoreModel.UI.CFormControlMgr;

public class COrder extends CBaseObject
{

    public COrder()
    {
        TbCode = "DD_Order";
        ClassName = "com.ErpCoreModel.Store.COrder";

    }

    
        public String getCode()
        {
            if (m_arrNewVal.containsKey("code"))
                return m_arrNewVal.get("code").StrVal;
            else
                return "";
        }
        public void setCode(String value)
        {
            if (m_arrNewVal.containsKey("code"))
                m_arrNewVal.get("code").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("code", val);
            } 
       }
        public UUID getKH_Customer_id()
        {
            if (m_arrNewVal.containsKey("kh_customer_id"))
                return m_arrNewVal.get("kh_customer_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setKH_Customer_id(UUID value)
        {
            if (m_arrNewVal.containsKey("kh_customer_id"))
                m_arrNewVal.get("kh_customer_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("kh_customer_id", val);
            }
        }
        public UUID getB_Province_id()
        {
            if (m_arrNewVal.containsKey("b_province_id"))
                return m_arrNewVal.get("b_province_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setB_Province_id(UUID value)
        {
            if (m_arrNewVal.containsKey("b_province_id"))
                m_arrNewVal.get("b_province_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("b_province_id", val);
            }
        }
        public UUID getB_City_id()
        {
            if (m_arrNewVal.containsKey("b_city_id"))
                return m_arrNewVal.get("b_city_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setB_City_id(UUID value)
        {
            if (m_arrNewVal.containsKey("b_city_id"))
                m_arrNewVal.get("b_city_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("b_city_id", val);
            }
        }
        public UUID getB_District_id()
        {
            if (m_arrNewVal.containsKey("b_district_id"))
                return m_arrNewVal.get("b_district_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setB_District_id(UUID value)
        {
            if (m_arrNewVal.containsKey("b_district_id"))
                m_arrNewVal.get("b_district_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("b_district_id", val);
            }
        }
        public String getAddr()
        {
            if (m_arrNewVal.containsKey("addr"))
                return m_arrNewVal.get("addr").StrVal;
            else
                return "";
        }
        public void setAddr(String value)
        {
            if (m_arrNewVal.containsKey("addr"))
                m_arrNewVal.get("addr").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("addr", val);
            } 
       }
        public String getZipcode()
        {
            if (m_arrNewVal.containsKey("zipcode"))
                return m_arrNewVal.get("zipcode").StrVal;
            else
                return "";
        }
        public void setZipcode(String value)
        {
            if (m_arrNewVal.containsKey("zipcode"))
                m_arrNewVal.get("zipcode").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("zipcode", val);
            } 
       }
        public String getContacts()
        {
            if (m_arrNewVal.containsKey("contacts"))
                return m_arrNewVal.get("contacts").StrVal;
            else
                return "";
        }
        public void setContacts(String value)
        {
            if (m_arrNewVal.containsKey("contacts"))
                m_arrNewVal.get("contacts").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("contacts", val);
            } 
       }
        public String getTel()
        {
            if (m_arrNewVal.containsKey("tel"))
                return m_arrNewVal.get("tel").StrVal;
            else
                return "";
        }
        public void setTel(String value)
        {
            if (m_arrNewVal.containsKey("tel"))
                m_arrNewVal.get("tel").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("tel", val);
            } 
       }
        public String getPhone()
        {
            if (m_arrNewVal.containsKey("phone"))
                return m_arrNewVal.get("phone").StrVal;
            else
                return "";
        }
        public void setPhone(String value)
        {
            if (m_arrNewVal.containsKey("phone"))
                m_arrNewVal.get("phone").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("phone", val);
            } 
       }
        public double getOtherCharge()
        {
            if (m_arrNewVal.containsKey("othercharge"))
                return m_arrNewVal.get("othercharge").DoubleVal;
            else
                return 0;
        }
        public void setOtherCharge(double value)
        {
            if (m_arrNewVal.containsKey("othercharge"))
                m_arrNewVal.get("othercharge").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("othercharge", val);
            }
        }
        public double getShipCharge()
        {
            if (m_arrNewVal.containsKey("shipcharge"))
                return m_arrNewVal.get("shipcharge").DoubleVal;
            else
                return 0;
        }
        public void setShipCharge(double value)
        {
            if (m_arrNewVal.containsKey("shipcharge"))
                m_arrNewVal.get("shipcharge").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("shipcharge", val);
            }
        }
        public double getDiscount()
        {
            if (m_arrNewVal.containsKey("discount"))
                return m_arrNewVal.get("discount").DoubleVal;
            else
                return 0;
        }
        public void setDiscount(double value)
        {
            if (m_arrNewVal.containsKey("discount"))
                m_arrNewVal.get("discount").DoubleVal = value;
            else
            {
                CValue val = new CValue();
                val.DoubleVal = value;
                m_arrNewVal.put("discount", val);
            }
        }
        public UUID getB_User_id()
        {
            if (m_arrNewVal.containsKey("b_user_id"))
                return m_arrNewVal.get("b_user_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setB_User_id(UUID value)
        {
            if (m_arrNewVal.containsKey("b_user_id"))
                m_arrNewVal.get("b_user_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("b_user_id", val);
            }
        }
        public String getRemarks()
        {
            if (m_arrNewVal.containsKey("remarks"))
                return m_arrNewVal.get("remarks").StrVal;
            else
                return "";
        }
        public void setRemarks(String value)
        {
            if (m_arrNewVal.containsKey("remarks"))
                m_arrNewVal.get("remarks").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("remarks", val);
            } 
       }
        public int getState()
        {
            if (m_arrNewVal.containsKey("state"))
                return m_arrNewVal.get("state").IntVal;
            else
                return 0;
        }
        public void setState(int value)
        {
            if (m_arrNewVal.containsKey("state"))
                m_arrNewVal.get("state").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("state", val);
            }
        }
        public String getComment()
        {
            if (m_arrNewVal.containsKey("comment"))
                return m_arrNewVal.get("comment").StrVal;
            else
                return "";
        }
        public void setComment(String value)
        {
            if (m_arrNewVal.containsKey("comment"))
                m_arrNewVal.get("comment").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("comment", val);
            } 
       }
        public int getScoring()
        {
            if (m_arrNewVal.containsKey("scoring"))
                return m_arrNewVal.get("scoring").IntVal;
            else
                return 0;
        }
        public void setScoring(int value)
        {
            if (m_arrNewVal.containsKey("scoring"))
                m_arrNewVal.get("scoring").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("scoring", val);
            }
        }
        public int getPayMode()
        {
            if (m_arrNewVal.containsKey("paymode"))
                return m_arrNewVal.get("paymode").IntVal;
            else
                return 0;
        }
        public void setPayMode(int value)
        {
            if (m_arrNewVal.containsKey("paymode"))
                m_arrNewVal.get("paymode").IntVal = value;
            else
            {
                CValue val = new CValue();
                val.IntVal = value;
                m_arrNewVal.put("paymode", val);
            }
        }
        

        public COrderDetailMgr getOrderDetailMgr()
        {
            return (COrderDetailMgr)this.GetSubObjectMgr("DD_OrderDetail", COrderDetailMgr.class);
        }
        public void setOrderDetailMgr(COrderDetailMgr value)
        {
            this.SetSubObjectMgr("DD_OrderDetail", value);
        }
}
